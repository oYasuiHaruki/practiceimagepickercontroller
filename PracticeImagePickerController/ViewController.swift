//
//  ViewController.swift
//  PracticeImagePickerController
//
//  Created by 安井春輝 on 11/6/30 H.
//  Copyright © 30 Heisei haruki yasui. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let profileImageButton: UIButton = {
        let button = UIButton(type: .system)
        //ここのimageは何でもいいです。
        button.setImage(UIImage(named: "plus_photo")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handlePlusPhoto), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc func handlePlusPhoto() {
        let imagePickerController = UIImagePickerController()
        //ユーザーが画像を選択したことが、伝わるようにするためにdelegateを設定
        imagePickerController.delegate = self
        //ユーザーが画像を選択後、サイズなどを編集できるようにするかどうか
        imagePickerController.allowsEditing = true
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImageButton.setImage(editedImage.withRenderingMode(.alwaysOriginal), for: .normal)
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageButton.setImage(originalImage.withRenderingMode(.alwaysOriginal), for: .normal)
        }

        //profileImageを円形にするため
        profileImageButton.layer.cornerRadius = profileImageButton.frame.width / 2
        profileImageButton.layer.masksToBounds = true
        profileImageButton.layer.borderColor = UIColor.black.cgColor
        profileImageButton.layer.borderWidth = 3

        //画像の選択画面を退けるために記載
        dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(profileImageButton)
        
        profileImageButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        profileImageButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        profileImageButton.widthAnchor.constraint(equalToConstant: 140).isActive = true
        profileImageButton.heightAnchor.constraint(equalToConstant: 140).isActive = true
    }
}

